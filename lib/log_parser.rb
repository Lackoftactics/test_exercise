require 'set'

class LogParser
  def initialize(log_path)
    self.log_path = log_path
  end

  def add_views
    File.foreach(log_path) do |line|
      url, ip = line.split(' ')

      total[url].push(ip)
      uniques[url].add(ip)
    end
  end

  def render_views_stats
    puts "Most page views"
    sorted_total.each do |url, ips|
      puts "#{url} #{ips.length} visits"
    end

    puts "============================================"

    puts "Most unique page view"
    sorted_uniques.each do |url, ips|
      puts "#{url} #{ips.length} unique views"
    end
  end

  class << self
    def show_stats(log_path)
      parser = new(log_path)
      parser.add_views
      parser.render_views_stats
    end
  end

  def sorted_total
    sort_url_stats(total)
  end

  def sorted_uniques
    sort_url_stats(uniques)
  end

  private

  attr_accessor :log_path

  def total
    @total ||= Hash.new { |h, k| h[k] = [] }
  end

  def uniques
    @uniques ||= Hash.new { |h, k| h[k] = Set.new }
  end

  def sort_url_stats(urls)
    urls.sort_by { |url, ips| ips.length }.reverse
  end
end
