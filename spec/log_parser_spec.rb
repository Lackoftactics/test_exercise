require 'spec_helper'

describe 'LogParser' do
  let(:test_file_path) { 'spec/fixtures/test.log' }

  it 'processes file' do
    log_parser = LogParser.new(test_file_path)
    log_parser.add_views

    #maybe better to test only exposed methods to interface
    total = log_parser.send(:total)
    uniques = log_parser.send(:uniques)
    expect(total['/contact'].length > 0).to eq(true)
    expect(uniques['/contact'].length > 0).to eq(true)
  end

  it 'has sorted total stats' do
    log_parser = LogParser.new(test_file_path)
    log_parser.add_views

    expect(log_parser.sorted_total.to_h['/home'].length).to eq(4)
  end

  it 'has sorted unique stats' do
    # these repetitions are on purpose, in long files before do blocks hurts
    log_parser = LogParser.new(test_file_path)
    log_parser.add_views

    expect(log_parser.sorted_uniques.to_h['/home'].length).to eq(3)
  end

  it 'shows full stats' do
    expected_stdout = <<-CONSOLE_OUTPUT
Most page views
/home 4 visits
/contact 3 visits
============================================
Most unique page view
/home 3 unique views
/contact 1 unique views
    CONSOLE_OUTPUT

    expect do
      LogParser.show_stats(test_file_path)
    end.to output(expected_stdout).to_stdout
  end
end
